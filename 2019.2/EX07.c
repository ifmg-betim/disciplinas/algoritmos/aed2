#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> 

struct sPessoa
{
	char * nome;
	int idade;
	float peso;
	float altura;
	float imc;
};

typedef struct sPessoa Pessoa;

Pessoa * mais_magra (Pessoa * p, int tamanho) {
	int i = 0, r = 0;
	float im = p[r].imc;
	
	for(i = r + 1; i < tamanho; i++) {
		
		if (p[i].imc < im){
			im = p[i].imc;
			r = i;
		}
	}
		
	return &p[r];
}

Pessoa * mais_gorda (Pessoa * p, int tamanho) {
	int i = 0, r = 0;
	float im = p[r].imc;
	
	for(i = r + 1; i < tamanho; i++) {
		
		if (p[i].imc > im){
			im = p[i].imc;
			r = i;
		}
	}
		
	return &p[r];
}

Pessoa * mais_alta (Pessoa * p, int tamanho) {
	int i = 0, r = 0;
	float im = p[r].altura;
	
	for(i = r + 1; i < tamanho; i++) {
		
		if (p[i].altura > im){
			im = p[i].altura;
			r = i;
		}
	}
		
	return &p[r];
}

Pessoa * mais_velha (Pessoa * p, int tamanho) {
	int i = 0, r = 0;
	int im = p[r].idade;
	
	for(i = r + 1; i < tamanho; i++) {
		
		if (p[i].idade > im){
			im = p[i].idade;
			r = i;
		}
	}
		
	return &p[r];
}

Pessoa * mais_nova (Pessoa * p, int tamanho) {
	
	int i = 0, r = 0;
	int im = p[r].idade;
	
	for(i = r + 1; i < tamanho; i++) {
		
		if (p[i].idade < im){
			im = p[i].idade;
			r = i;
		}
	}
		
	return &p[r];
}

int main () {
	
	Pessoa * p = malloc(sizeof(Pessoa));

	char nome[100];
	int idade = 0;
	float peso = 0.0;
	float altura = 0.0;

	int nPessoas = 0;

	scanf("%i", &nPessoas);

	int count = 0, i = 0;
	for(i = 0; i < nPessoas; i++) {

		scanf("%s %i %f %f", nome, &idade, &peso, &altura);
		
		count++;
		
		p = realloc(p, sizeof(Pessoa) * count);

		p[i].nome = malloc(sizeof(char) * 100);
		
		i = (count - 1);

		strcpy(p[i].nome, nome);
		p[i].idade = idade;
		p[i].peso = peso;
		p[i].altura = altura;

		p[i].imc = p[i].peso / pow(p[i].altura, 2);
	}

	Pessoa * maisMagra = mais_magra (p, count);
	Pessoa * maisGorda = mais_gorda (p, count);
	Pessoa * maisAlta  = mais_alta  (p, count);
	Pessoa * maisVelha = mais_velha (p, count);
	Pessoa * maisNova  = mais_nova  (p, count);

	printf("MAIS MAGRA: %s\n", maisMagra->nome);
	printf("MAIS GORDA: %s\n", maisGorda->nome);
	printf("MAIS ALTA : %s\n", maisAlta->nome);
	printf("MAIS VELHA: %s\n", maisVelha->nome);
	printf("MAIS NOVA : %s\n", maisNova->nome);

	free(p);
	p = NULL;
	
	return 0;
}
