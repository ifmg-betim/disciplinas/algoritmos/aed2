#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef struct sNo No;

struct sNo {

	No * proximo;
	int numero;
};

typedef struct {

	No * inicio;
	No * final;

	int tamanho;

} Lista;

No * criarNo (int numero) {
	No * n = malloc(sizeof(No));
	n->proximo = NULL;
	n->numero = numero;

	return n;
}

No * andaPara (Lista * l, int x) {
	
	No * atual = NULL;
	int i = 0;
	for (i = 0, atual = l->inicio; i != x; atual = atual->proximo, i++) {}
	return atual;
}

Lista * criar() {

	Lista * l = malloc(sizeof(Lista));
	l->tamanho = 0;
	l->inicio = NULL;
	l->final = NULL;

	return l;
}

int remover_final (Lista * l) {
	
	int v = 0;

	No * atual = NULL;
	
	if (l->inicio) {

		if (l->tamanho > 1) {

			atual = andaPara(l, (l->tamanho - 2));
			atual->proximo = NULL;
			v = l->final->numero;
			free(l->final);
			l->final = atual;
		}
		else if (l->tamanho == 1) {

			atual = l->inicio;
			l->final = NULL;
			v = atual->numero;
			free(atual);
			l->inicio = NULL;
		}

		l->tamanho--;
	}

	return v;
}


int remover_inicio (Lista * l) {
	
	int v = 0;
	No * aux = NULL;

	if (l->inicio) {
		if (l->tamanho > 1) {
			aux = l->inicio;
			l->inicio = l->inicio->proximo;
			v = aux->numero;
			free(aux);
		}
		else if (l->tamanho == 1) {
			aux = l->inicio;
			v = aux->numero;
			
			free(aux);

			l->inicio = NULL;
			l->final = NULL;
		}

		l->tamanho--;
	}

	return v;
}


int remover (Lista * l, int x) {
	
	int r = 0;

	if (x >= 0 && x <= l->tamanho) {

		if (x == 0) {
			r = remover_inicio (l);
		}
		else if (x == l->tamanho) {
			r = remover_final(l);
		}
		else {
			
			No * anterior = andaPara(l, x - 1);
			No *  aux = anterior->proximo;

			anterior->proximo = aux->proximo;
			
			r = aux->numero;

			free(aux);

			l->tamanho--;
		}
	}
	else {
		printf("Posicao %i esta fora da lista.\n", x);
	}

	return r;
}

void inserir_final (Lista * l, int numero) {
	
	No * n = criarNo(numero);

	if(l->final != NULL) {
		l->final->proximo = n;
	}
	
	l->final = n;
	
	if (l->inicio == NULL) {
		l->inicio = l->final;
	}
	
	l->tamanho++;
}

int main(int argc, char const *argv[]) {
	
    int i = 0;
    int N, M;
    int numero = 0;

	Lista * ld = criar();
	Lista * ls = criar();

    scanf("%i %i", &N, &M);

    for(i = 0; i < N; i++)
    {
        scanf("%i", &numero);
        inserir_final(ld, numero);
    }

	srand(time(NULL));
    
    int is = 0;

	for (i = 0; i < M; i++, N--) {

		is = rand() % N;

		numero = remover(ld, is);
		inserir_final(ls, numero);
	}

	FILE * arq = fopen("sorteados.txt", "wt");

    printf("SORTEADOS: ");
    No * e = NULL;
    for (e = ls->inicio; e != NULL; e = e->proximo) {

		if (arq) {
			fprintf(arq, "%i ", e->numero);
		}
		printf("%i, ", e->numero);
	}

	if (arq) {
		fclose(arq);
	}

    printf("\nNAO SORTEADOS: ");
    for (e = ld->inicio; e != NULL; e = e->proximo) {
		printf("%i, ", e->numero);
	}

	return 0;
}

