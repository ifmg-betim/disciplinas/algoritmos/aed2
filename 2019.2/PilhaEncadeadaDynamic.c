#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
// Estrutura de Pilha Encadeada.
typedef struct no {
	struct no * proximo;
	int valor;
} node;

node * newNo (int v) {
	node * no = malloc(sizeof(node));
	no->proximo = NULL;
	no->valor = v;
}

typedef struct structPilha { 
	int tamanho; 
	node * topo; 
} Pilha;

// Cria uma pilha
Pilha * criar () { 
	
	Pilha * p = malloc(sizeof(Pilha)); 
	
	if(p) {
		p->topo = NULL;
		p->tamanho = 0;
	}
	
	return p;
} 

// Empilha um elemento; conhecido como metodo Push da Pilha
void empilha(Pilha * p, int A){
	
	if(p) {
		
		node * no = newNo(A);
		
		no->proximo = p->topo;
		p->topo = no;
		p->tamanho++;
	}
} 

int recuperaTopo (Pilha * p) {
	
	if(p && p->tamanho > 0) {
		return p->topo->valor;
	}
	
	return 0;
}


// Desemplilha um elemento; conhecido como metodo Pop da Pilha
int desempilha(Pilha * p) {
	
	if(p && p->tamanho > 0) {
		
		int r = p->topo->valor;
		
		node * aux = p->topo;
		p->topo = p->topo->proximo;
		free(aux);
		p->tamanho--;
		
		return r; 
	}
	
	return 0;
}

// Retorna o tamanho da pilha
int tamanho(Pilha * p) { 
	
	if(p) {
		return p->tamanho;
	}
	
	return 0;
} 

// Destroi uma pilha
void destruir(Pilha * p) { 

	if(p && p->tamanho > 0) {
		
		while (tamanho(p)) {
			desempilha(p);
		}
		
		free(p);
		p = NULL;
	}
}

void exibirPilha (Pilha * p) {
	
	printf("{");
	
	if(p && p->tamanho > 0) {
		
		int i = 0;
		node * aux = p->topo;
		
		for (i = 0; i < p->tamanho; i++) {
			if (i < p->tamanho - 1) {
				printf(" %i,", aux->valor);
			}
			else {
				printf(" %i ", aux->valor);
			}
			aux = aux->proximo;
		}
	}
	
	printf("}\n");
}


int main()
{
	Pilha * stack = criar();
	int passo = 1;
	
	printf("Passo %2i: ", passo++);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 5);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 6);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 8);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 78);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %2i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 51);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %2i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %2i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 12);
	exibirPilha(stack);
	
	printf("\nTamanho: %i\n\n", tamanho(stack));
	
	printf("[DESEMPILHA]\n");
	printf("Passo %i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	destruir(stack);
	
	return 0;
}
